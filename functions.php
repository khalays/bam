<?php
/**
 * bam functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bam
 */

if ( ! function_exists( 'bam_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function bam_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on bam, use a find and replace
	 * to change 'bam' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'bam', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'bam' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'bam_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'bam_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bam_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bam_content_width', 640 );
}
add_action( 'after_setup_theme', 'bam_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bam_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'bam' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'bam' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'bam_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bam_scripts() {
	wp_enqueue_style( 'bam-style', get_template_directory_uri() . '/assets/css/style.css', array(), '20170126' );

	wp_enqueue_script( 'bam-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'bam-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bam_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// Option page
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Réglages principaux',
		'menu_title'	=> 'Theme',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Coordonnées',
		'menu_title'	=> 'Coordonnées',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Configuration',
		'menu_title'	=> 'Configuration',
		'parent_slug'	=> 'theme-general-settings',
	));

}


add_action( 'init', 'codex_seance_init' );
/**
 * Register séance post type
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_seance_init() {
	$labels = array(
		'name'               => _x( 'Séances', 'post type general name', 'bam' ),
		'singular_name'      => _x( 'Séance', 'post type singular name', 'bam' ),
		'menu_name'          => _x( 'Séances', 'admin menu', 'bam' ),
		'name_admin_bar'     => _x( 'Séance', 'add new on admin bar', 'bam' ),
		'add_new'            => _x( 'Ajouter une séance', 'book', 'bam' ),
		'add_new_item'       => __( 'Ajouter une nouvelle séance', 'bam' ),
		'new_item'           => __( 'Nouvelle séance', 'bam' ),
		'edit_item'          => __( 'Editer la séance', 'bam' ),
		'view_item'          => __( 'Voir la séance', 'bam' ),
		'all_items'          => __( 'Toutes les séances', 'bam' ),
		'search_items'       => __( 'Rechercher les scéances', 'bam' ),
		'parent_item_colon'  => __( 'Séance Parente:', 'bam' ),
		'not_found'          => __( 'Aucune séance trouvée.', 'bam' ),
		'not_found_in_trash' => __( 'Aucune séance trouvée dans la corbeille.', 'bam' )
	);

	$args = array(
		'labels'             => $labels,
    'description'        => __( 'Description.', 'bam' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'seance' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'					 => 'dashicons-cloud',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
	);

	register_post_type( 'seance', $args );
}

add_action( 'init', 'codex_instructeur_init' );
/**
 * Register Instructeur post type
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_instructeur_init() {
	$labels = array(
		'name'               => _x( 'Instructeurs', 'post type general name', 'bam' ),
		'singular_name'      => _x( 'Instructeur', 'post type singular name', 'bam' ),
		'menu_name'          => _x( 'Instructeurs', 'admin menu', 'bam' ),
		'name_admin_bar'     => _x( 'Instructeur', 'add new on admin bar', 'bam' ),
		'add_new'            => _x( 'Ajouter un instructeur', 'book', 'bam' ),
		'add_new_item'       => __( 'Ajouter un nouvel instructeur', 'bam' ),
		'new_item'           => __( 'Nouvel instructeur', 'bam' ),
		'edit_item'          => __( 'Editer l\'instructeur', 'bam' ),
		'view_item'          => __( 'Voir l\'instructeur', 'bam' ),
		'all_items'          => __( 'Tous les instructeurs', 'bam' ),
		'search_items'       => __( 'Rechercher les instructeurs', 'bam' ),
		'parent_item_colon'  => __( 'Instructeur Parent:', 'bam' ),
		'not_found'          => __( 'Aucun instructeur trouvé.', 'bam' ),
		'not_found_in_trash' => __( 'Aucun instructeur trouvé dans la corbeille.', 'bam' )
	);

	$args = array(
		'labels'             => $labels,
    'description'        => __( 'Description.', 'bam' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'instructeur' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'					 => 'dashicons-welcome-learn-more',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
	);

	register_post_type( 'instructeur', $args );
}

add_image_size( 'instructeur', 320, 320, true );


add_filter('acf/settings/save_json', 'my_acf_json_save_point');

function my_acf_json_save_point( $path ) {
    // update path
    $path = get_stylesheet_directory() . '/acf-json';
    // return
    return $path;
}

# custom admin login logo
function Ktheme_login_logo () {
	?>
	<style>
		body.login #login h1 a {
			background: url( '<?=get_template_directory_uri();?>/assets/img/bam-map-icon.png') 0 0 no-repeat transparent;
			width:94px;
			height:130px;
		}
	</style>
	<?
}
add_action( 'login_head', 'Ktheme_login_logo' );


// Changer le lien du logo 
add_filter( 'login_headerurl', 'Ktheme__loginlogo_url' );
function Ktheme__loginlogo_url($url) {
	return home_url();
}


// Signature Anamorphik dans l'admin en bas de pag
function Ktheme_custom_admin_footer () {
$signature = '<svg version="1.1" class="K-signature" id="K-signature" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-421.5 510.5 156 20" preserveAspectRatio="xMinYMin meet"><style type="text/css"><![CDATA[ .K-signature { display: inline-block; width: 191px; } .K-signature .K--texts { font: inherit; font-size: 10px; font-style: normal; fill: #363B3F; } .K-signature .K--front { fill:#C7341C; } .K-signature .K--back { fill: #ffffff; } .K-signature:focus .K--front, .K-signature:hover .K--front { fill:#C7341C; } .K-signature:focus .K--back, .K-signature:hover .K--back { fill: #ffffff; } .K-signature .K--link { text-decoration: none; } ]]></style><a class="K--link" xlink:href="http://www.anamorphik.com/" target="_blank"><g class="K--logo"><ellipse class="K--back" cx="-345.8" cy="520.5" rx="8.9" ry="9"/><path class="K--front" d="M-345.8,511.5c-0.6,0-1.2,0.1-1.8,0.2v14.5c0,0.2-0.2,0.4-0.4,0.4h-1.9c-0.2,0-0.4-0.2-0.4-0.4 v-13.5c-2.7,1.6-4.5,4.5-4.5,7.8c0,5,4,9,8.9,9c4.9,0,8.9-4,8.9-9S-340.9,511.5-345.8,511.5z M-341.5,523.6l1,1.2 c0.1,0.2,0.1,0.4,0,0.5l-1.4,1.2c-0.2,0.1-0.4,0.1-0.5,0l-1-1.2c-1.2-1.4-2.6-3.5-2.6-4.8l0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0 c0-1.3,1.4-3.4,2.6-4.8l1-1.2c0.1-0.2,0.4-0.2,0.5,0l1.4,1.2c0.2,0.1,0.2,0.4,0,0.5l-1,1.2c-1.5,1.8-2,2.4-2,3.1 C-343.4,521.2-343,521.8-341.5,523.6z"/></g><g class="K--texts"><text class="K--anamorphik" transform="matrix(1.033 0 0 1 -330.9302 523.8887)">anamorphik</text><text class="K--conception" transform="matrix(1.033 0 0 1 -415.4414 523.8887)">conception</text></g></a></svg>';
_e( '<span id="footer-thankyou">' . $signature . '</span>' );
}
add_filter ('admin_footer_text', 'Ktheme_custom_admin_footer');