<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bam
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="main-wrapper">
			<section class="footer__info">
				<div class="f__info__logo">
					<img src="<?php echo get_template_directory_uri() . '/assets/img/bam-logo-icon.png';  ?>" alt="Bar à méditation">
				</div>
				<div class="f__info__coord">
					5 rue Gaillon <br>
					75002 Paris<br>
					Tél : <?php the_field('telephone', 'option'); ?>
				</div>
			</section>
			<section class="footer__menu">
				<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'footer-menu' ) ); ?>
			</section>
			<section class="footer__newsletter">
				Inscrivez-vous à notre newsletter pour recevoir les dernières actualités, nos offres spéciales, et les événements.
				<?php echo do_shortcode(get_field('newsletter_shortcode', 'option')); ?>
			</section>
		</div>
		<section class="footer__cred">
			<ul>
				<li>Tous droits réservés Barameditation.fr</li>
				<li><a href="<?php the_field('cgv', 'option'); ?>">Informations légales</a></li>
				<li><a href="http://www.anamorphik.com/" target="_blank" class="anamorphik-sign"><img src="<?php echo get_theme_file_uri( 'assets/img/k-signature-inline.svg' ); ?>" alt="Anamorphik"></a></li>
			</ul>
		</section>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
