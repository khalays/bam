<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bam
 */

$post_id = get_the_ID();
?>
<?php if ( has_post_thumbnail() ):  ?>
<div class="p__hero" style="background-image:url('<?php the_post_thumbnail_url( 'full' ); ?>');">
	<div class="main-wrapper">
		<header>
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>
	</div>
</div>
<?php endif; ?>

<div class="main-wrapper">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( !has_post_thumbnail() ):  ?>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
	<?php endif; ?>
	<div class="entry-content">
		<div class="tarif__bloc_texte">
			<?php
				the_field('tarif__bloc_texte');
			?>
		</div>
		<section class="pricing-table">
			<?php if( have_rows('table_de_prix') ): ?>
				<ul>
				<?php while( have_rows('table_de_prix') ): the_row();  ?>
					<li>
						<div class="pricing__title"><span><?php the_sub_field('tarif__titre'); ?></span></div>
						<div class="pricing__price"><?php the_sub_field('tarif__prix'); ?></div>
						<div class="pricing__cta"><a href="<?php the_sub_field('tarif__btn_lien'); ?>" class='btn'><?php the_sub_field('tarif__btn_texte'); ?></a></div>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</section>

		<div class="tarif__bloc_texte">
			<?php
				the_field('tarif__bloc_texte_2');
			?>
		</div>
	</div><!-- .entry-content -->



</article><!-- #post-## -->
</div>
