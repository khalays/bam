<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bam
 */

$post_id = get_the_ID();
?>
<?php if ( has_post_thumbnail() ):  ?>
<div class="p__hero" style="background-image:url('<?php the_post_thumbnail_url( 'full' ); ?>');">
	<div class="main-wrapper">
		<header>
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>
	</div>
</div>
<?php endif; ?>

<div class="main-wrapper">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( !has_post_thumbnail() ):  ?>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
	<?php endif; ?>
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bam' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<section class="equipe">
		<ul>
			<?php
			$args = array( 'post_type' => 'instructeur', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC' );
			$seances = new WP_Query( $args );
			while ( $seances->have_posts() ) : $seances->the_post(); ?>
				<li>
					<div class="equipe__image"><img src="<?php the_post_thumbnail_url( 'instructeur' ); ?>" alt="<?php the_title(); ?>"></div>
					<div class="equipe__content-wrap">
						<div class="equipe__titre"><h3><?php the_title(); ?></h3></div>
						<div class="equipe__content"><?php the_content(); ?></div>
					</div>
				</li>
			<?php endwhile; ?>
		</ul>
	</section>

</article><!-- #post-## -->
</div>
