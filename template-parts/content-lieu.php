<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bam
 */

$post_id = get_the_ID();
?>
<?php if ( has_post_thumbnail() ):  ?>
<div class="p__hero" style="background-image:url('<?php the_post_thumbnail_url( 'full' ); ?>');">
	<div class="main-wrapper">
		<header>
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>
	</div>
</div>
<?php endif; ?>
<div class="main-wrapper">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( !has_post_thumbnail() ):  ?>
		<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
	<?php endif; ?>
	<div class="entry-content">
			<div class="lieu__bloc_texte">
				<?php
					the_content();
				?>
			</div>

			<?php if( have_rows('etage') ): ?>

				<section class="lieu__levels">
					<ul>
						<?php $cpt = 0; ?>
					<?php while( have_rows('etage') ): the_row();
						$cpt++;
						// vars
						$image = get_sub_field('lieu__image');
						?>
						<?php if ($cpt % 2 != 0): ?>
						<li>
							<div class="lieu__description">
								<h3><?php the_sub_field('lieu__titre') ?></h3>
								<?php the_sub_field('lieu__description') ?>
							</div>
							<div class="lieu__img" style="background-image:url(<?php echo $image['sizes']['large']; ?>);">
							</div>
						</li>
					<?php else: ?>
						<li>
							<div class="lieu__img" style="background-image:url(<?php echo $image['sizes']['large']; ?>);">
							</div>
							<div class="lieu__description">
								<h3><?php the_sub_field('lieu__titre') ?></h3>
								<?php the_sub_field('lieu__description') ?>
							</div>
						</li>
					<?php endif; ?>

					<?php endwhile; ?>

					</ul>
				</section>

			<?php endif; ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
</div>
