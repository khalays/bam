<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bam
 */


$post_id = get_the_ID();
?>
<div class="h__hero" style="background-image:url('<?php the_field('h__hero_banner_background', $post_id); ?>');">
	<div class="main-wrapper">
		<header>
			<?php the_field('h__hero_banner_text', $post_id); ?>
		</header>
		<div class="hero__btn">
			<p><a class="btn btn--large btn--light btn--inverted" href="<?php the_field('h__hero_banner_btn1_lien', $post_id); ?>"><?php the_field('h__hero_banner_btn1_texte', $post_id); ?></a></p>
			<p><a class="btn btn--large" href="<?php the_field('h__hero_banner_btn2_lien', $post_id); ?>" target="_blank"><?php the_field('h__hero_banner_btn2_texte', $post_id); ?></a></p>
		</div>
	</div>
</div>
<div class="main-wrapper">
	<section class="h__intro">
			<?php the_field('h__texte_introduction', $post_id); ?>
	</section>
</div>
<section class="h__seances">
	<div class="main-wrapper">
		<h2>
			<span>Les séances</span>
		</h2>

		<ul>
			<?php
			$args = array( 'post_type' => 'seance', 'posts_per_page' => 8, 'orderby' => 'menu_order', 'order' => 'ASC' );
			$seances = new WP_Query( $args );
			while ( $seances->have_posts() ) : $seances->the_post(); ?>
				<li><a href="<?php the_field('h__seances_lien', $post_id); ?>"><span class="seances__icon"><?php the_post_thumbnail( 'full' ); ?></span><p class="seances__title"><?php the_title(); ?></p></a></li>
			<?php endwhile; ?>
		</ul>
		<p><a href="<?php the_field('h__seances_lien', $post_id); ?>" class="btn btn--light"><?php the_field('h__seances_texte', $post_id); ?></a></p>
	</div>
</section>
<section class="h__inter" style="background-image:url('<?php the_field('h__lieu_banner_background', $post_id); ?>');">
	<div class="main-wrapper">
		<div class="inter__content"><?php the_field('h__lieu_banner_texte', $post_id); ?></div>
			<a href="<?php the_field('h__lieu_btn_lien', $post_id); ?>" class="btn btn--light btn--inverted btn--large"><?php the_field('h__lieu_btn_texte', $post_id); ?></a>
	</div>
</section>
<div class="full-width h__map-access">
	<section class="h__map">
		<div id="map"></div>
    <script>
      function initMap() {
        var bam = {lat: 48.8683099, lng: 2.333970200000067};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: bam,
					scrollwheel: false
        });
				var image = {
			    url: '<?php echo get_template_directory_uri() . '/assets/img/bam-map-icon.png'; ?>',
					scaledSize: new google.maps.Size(47, 65),
			    // The origin for this image is (0, 0).
			    origin: new google.maps.Point(0, 0),
			    // The anchor for this image is the base of the flagpole at (0, 32).
			    anchor: new google.maps.Point(23, 65)
			  };
				// var image = '<?php echo get_template_directory_uri() . '/assets/img/bam-map-icon.png'; ?>';
			  var bamMarker = new google.maps.Marker({
			    position: bam,
			    map: map,
			    icon: image,
					title: 'Bar à méditation'
			  });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAT0lFRaRlhrCgzfaDFi5xh2I6DpDz_jSA&callback=initMap">
    </script>
	</section>
	<section class="h__access">
		<div class="access__horaires">
			<strong>Horaires d’ouverture :</strong> <?php the_field('horaires', 'option'); ?>
		</div>
		<div class="access__phone">
			<strong>Téléphone :</strong> <?php the_field('telephone', 'option'); ?>
		</div>
		<div class="access__address">
				<strong>Adresse :</strong> <?php the_field('adresse', 'option'); ?>
		</div>
		<section class="access__transport">
			<div class="transport__metro">
				<div class="transport__title"><strong>Métro :</strong></div>
				<div class="transport__items">
					<?php if( have_rows('metro', 'option') ): ?>
				    <ul>
				    <?php while( have_rows('metro', 'option') ): the_row(); ?>
				        <li><?php the_sub_field('ligne'); ?></li>
				    <?php endwhile; ?>
				    </ul>
					<?php endif; ?>
				</div>
			</div>
			<div class="transport__bus">
				<div class="transport__title"><strong>Bus :</strong></div>
				<div class="transport__items">
					<?php if( have_rows('bus', 'option') ): ?>
						<ul>
						<?php while( have_rows('bus', 'option') ): the_row(); ?>
								<li><?php the_sub_field('ligne'); ?></li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
			<div class="transport__rer">
					<div class="transport__title"><strong>RER :</strong></div>
					<div class="transport__items">Station Auber RER A</div>
			</div>
			<div class="transport__parking">
					<strong>Parking Pyramides :</strong> <?php the_field('parking', 'option'); ?>
			</div>
			<div class="transport__voiturier">
					<strong>Voiturier :</strong> <?php the_field('voiturier', 'option'); ?>
			</div>
		</section>
	</section>
</div>

<section class="h__equipe">
	<div class="main-wrapper">
		<h2><span>L'équipe</span></h2>
		<?php the_field('h__equipe_texte', $post_id); ?>
		<ul>
			<?php
			$args = array( 'post_type' => 'instructeur', 'posts_per_page' => 4, 'meta_key' => 'instructeur_mise_en_avant', 'meta_value'	=> '1', 'orderby' => 'menu_order', 'order' => 'ASC' );
			$seances = new WP_Query( $args );
			while ( $seances->have_posts() ) : $seances->the_post(); ?>
				<li>
					<?php the_post_thumbnail( 'instructeur' ); ?>
					<p><?php the_title(); ?></p>
				</li>
			<?php endwhile; ?>
		</ul>
		<p><a href="<?php the_field('h__equipe_btn_lien', $post_id); ?>" class="btn btn--light"><?php the_field('h__equipe_btn_texte', $post_id); ?></a></p>
	</div>
</section>

<section class="h__salle" style="background-image:url('<?php the_field('h__interstitiel_image', $post_id); ?>');">

</section>
<section class="h__tarif">
	<div class="main-wrapper">
		<h2><span>Les tarifs</span></h2>
		<div class="tarif__content">
			<?php the_field('h__tarifs_texte', $post_id); ?>
		</div>
		<p>
			<a href="<?php the_field('h__tarifs_btn1_lien', $post_id); ?>" class="btn btn--light"><?php the_field('h__tarifs_btn1_texte', $post_id); ?></a>
			<a href="<?php the_field('h__tarifs_btn2_lien', $post_id); ?>" class="btn"><?php the_field('h__tarifs_btn2_texte', $post_id); ?></a>
		</p>
	</div>
</section>
