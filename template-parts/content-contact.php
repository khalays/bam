<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bam
 */


$post_id = get_the_ID();
?>

<div class="full-width h__map-access">
	<section class="h__map">
		<div id="map"></div>
    <script>
      function initMap() {
        var bam = {lat: 48.8683099, lng: 2.333970200000067};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: bam,
					scrollwheel: false
        });
				var image = {
			    url: '<?php echo get_template_directory_uri() . '/assets/img/bam-map-icon.png'; ?>',
					scaledSize: new google.maps.Size(47, 65),
			    // The origin for this image is (0, 0).
			    origin: new google.maps.Point(0, 0),
			    // The anchor for this image is the base of the flagpole at (0, 32).
			    anchor: new google.maps.Point(23, 65)
			  };
				// var image = '<?php echo get_template_directory_uri() . '/assets/img/bam-map-icon.png'; ?>';
			  var bamMarker = new google.maps.Marker({
			    position: bam,
			    map: map,
			    icon: image,
					title: 'Bar à méditation'
			  });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAT0lFRaRlhrCgzfaDFi5xh2I6DpDz_jSA&callback=initMap">
    </script>
	</section>
	<section class="h__access">
		<div class="access__horaires">
			<strong>Horaires d’ouverture :</strong> <?php the_field('horaires', 'option'); ?>
		</div>
		<div class="access__phone">
			<strong>Téléphone :</strong> <?php the_field('telephone', 'option'); ?>
		</div>
		<div class="access__address">
				<strong>Adresse :</strong> <?php the_field('adresse', 'option'); ?>
		</div>
		<section class="access__transport">
			<div class="transport__metro">
				<div class="transport__title"><strong>Métro :</strong></div>
				<div class="transport__items">
					<?php if( have_rows('metro', 'option') ): ?>
				    <ul>
				    <?php while( have_rows('metro', 'option') ): the_row(); ?>
				        <li><?php the_sub_field('ligne'); ?></li>
				    <?php endwhile; ?>
				    </ul>
					<?php endif; ?>
				</div>
			</div>
			<div class="transport__bus">
				<div class="transport__title"><strong>Bus :</strong></div>
				<div class="transport__items">
					<?php if( have_rows('bus', 'option') ): ?>
						<ul>
						<?php while( have_rows('bus', 'option') ): the_row(); ?>
								<li><?php the_sub_field('ligne'); ?></li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
			<div class="transport__rer">
					<div class="transport__title"><strong>RER :</strong></div>
					<div class="transport__items">Station Auber RER A</div>
			</div>
			<div class="transport__parking">
					<strong>Parking Pyramides :</strong> <?php the_field('parking', 'option'); ?>
			</div>
			<div class="transport__voiturier">
					<strong>Voiturier :</strong> <?php the_field('voiturier', 'option'); ?>
			</div>
		</section>
	</section>
</div>

<section class="contact-form">
	<div class="main-wrapper">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="entry-content">
			<?php
				the_content();

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bam' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
		<div class="cf__wrap">
			<?php echo do_shortcode(get_field('cf_shortcode')); ?>
		</div>
	</div>
</section>
